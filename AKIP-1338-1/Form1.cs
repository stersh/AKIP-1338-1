﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TekVISANet;

namespace AKIP_1338_1
{
    public partial class Form1 : Form
    {
        AKIPFuncs akip = new AKIPFuncs();
        private ArrayList devicesList = new ArrayList();

        private bool connection = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            devicesList.Clear();
            devicesList.AddRange(akip.getDevices());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            devicesList.AddRange(akip.getDevices());
            devices.DataSource = devicesList;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            akip.Device = devices.SelectedText;
            if (voltage.Text.Contains("-") && voltageStep.Text.Length > 0)
            {

                string[] voltageMas = voltage.Text.Trim().Split('-');
                int voltageStepCount = Int32.Parse(voltageStep.Text);
                int minVoltage = Int32.Parse(voltageMas[0]);
                int maxVoltage = Int32.Parse(voltageMas[1]);

                for (int i = minVoltage; i <= maxVoltage; i++)
                {
                    int rowNumber = measure_results.Rows.Add();
                    akip.Voltage = i.ToString();
                    measure_results.Rows[rowNumber].Cells["v"].Value = akip.Voltage;
                    measure_results.Rows[rowNumber].Cells["a"].Value = akip.Current;
                }
            }
            if(voltage.Text.Contains(','))
            {
                string[] voltageMas = voltage.Text.Trim().Split(',');
                for (int i = 0; i <= voltageMas.Length; i++)
                {
                    int rowNumber = measure_results.Rows.Add();
                    akip.Voltage = voltageMas[i];
                    measure_results.Rows[rowNumber].Cells["v"].Value = akip.Voltage;
                    measure_results.Rows[rowNumber].Cells["a"].Value = akip.Current;
                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Console.WriteLine("Start");
           if(connection)
            {
                connect.Text = "Подключиться";
                devices.Enabled = true;
                scan.Enabled = true;
                if(akip.AkipVisa.Open(akip.Device))
                {
                    akip.Remote = false;
                    connection = false;
                }
            }
            else
            {
                connect.Text = "Отключиться";
                akip.Device = devices.SelectedText;
                devices.Enabled = false;
                scan.Enabled = false;
                if (akip.AkipVisa.Open(akip.Device))
                {
                    akip.Remote = true;
                    connection = true;
                }
                Console.WriteLine("Done");
            }
        }

        private void importCsv_Click(object sender, EventArgs e)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("vc.csv");
            file.WriteLine(log.Text);
            file.Close();
        }

        private void ModeSelector_Selected(object sender, TabControlEventArgs e)
        {
            if(ModeSelector.SelectedTab == VOLTage_tab)
            {
                akip.Mode = "VOLTage";
            }
            if(ModeSelector.SelectedTab == CURRent_tab)
            {
                akip.Mode = "CURRent";
            }
        }
    }
}
