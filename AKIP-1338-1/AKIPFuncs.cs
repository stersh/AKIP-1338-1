﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using TekVISANet;

namespace AKIP_1338_1
{
    class AKIPFuncs
    {
        public VISA AkipVisa;

        public AKIPFuncs()
        {
            AkipVisa = new VISA();      
        }

        public string Device { get; set; }

        public bool OpenDevice()
        {
            return AkipVisa.Open(Device);
        }

        public void CloseDevice()
        {
            if (AkipVisa != null)
            {
                AkipVisa.Close();
            }
        }

        public ArrayList getDevices()
        {
            ArrayList resources = new ArrayList();
            AkipVisa.FindResources("?*", out resources);
            return resources;
        }

        public string Current
        {
            set
            {
                 AkipVisa.Write(String.Format("CURR {0}", value));
            }

            get
            {
                 string response;
                 AkipVisa.Query("MEASure:CURRent?", out response);
                 return response;
            }
        }

        public string Voltage
        {
            set
            {
                AkipVisa.Write(String.Format("VOLT {0}", value));
            }

            get
            {
                string response;
                AkipVisa.Query("VOLT?", out response);
                return response;
            }
        }

        public bool Remote
        {
            set {
                if(value)
                {
                    AkipVisa.Write("SYSTem:REMote");
                }
                else
                {
                    AkipVisa.Write("SYSTem:LOCal");
                }
            }

            get {
                return Remote;
            }
        }

        public bool Input
        {
            set
            {
                if(value)
                {
                    AkipVisa.Write("SOURce:INPut ON");
                }
                else
                {
                    AkipVisa.Write("SOURce:INPut OFF");
                }
            }
            get
            {
                string response;
                AkipVisa.Query("SOURce:INPut:STATe?", out response);
                if(response.Equals("1"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string Mode
        {
            get
            {
                string response;
                AkipVisa.Query("SOURce:FUNCtion?", out response);
                return response;
            }
            set
            {
                AkipVisa.Write(String.Format("SOURce:FUNCtion", value));
            }
        }
    }
}
