﻿namespace AKIP_1338_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.scan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.devices = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.status_logger = new System.Windows.Forms.ToolStripStatusLabel();
            this.status_progressbar = new System.Windows.Forms.ToolStripProgressBar();
            this.voltage = new System.Windows.Forms.TextBox();
            this.measure = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.voltageStep = new System.Windows.Forms.TextBox();
            this.voltageStepLable = new System.Windows.Forms.Label();
            this.connect = new System.Windows.Forms.Button();
            this.importCsv = new System.Windows.Forms.Button();
            this.ModeSelector = new System.Windows.Forms.TabControl();
            this.VOLTage_tab = new System.Windows.Forms.TabPage();
            this.measure_results = new System.Windows.Forms.DataGridView();
            this.v = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRent_tab = new System.Windows.Forms.TabPage();
            this.statusStrip1.SuspendLayout();
            this.ModeSelector.SuspendLayout();
            this.VOLTage_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measure_results)).BeginInit();
            this.SuspendLayout();
            // 
            // scan
            // 
            this.scan.Location = new System.Drawing.Point(142, 25);
            this.scan.Name = "scan";
            this.scan.Size = new System.Drawing.Size(82, 21);
            this.scan.TabIndex = 0;
            this.scan.Text = "Сканировать";
            this.scan.UseVisualStyleBackColor = true;
            this.scan.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Доступные устройства:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // devices
            // 
            this.devices.FormattingEnabled = true;
            this.devices.Location = new System.Drawing.Point(12, 25);
            this.devices.Name = "devices";
            this.devices.Size = new System.Drawing.Size(124, 21);
            this.devices.TabIndex = 2;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status_logger,
            this.status_progressbar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 457);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(725, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // status_logger
            // 
            this.status_logger.Name = "status_logger";
            this.status_logger.Size = new System.Drawing.Size(43, 17);
            this.status_logger.Text = "Статус";
            // 
            // status_progressbar
            // 
            this.status_progressbar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.status_progressbar.Name = "status_progressbar";
            this.status_progressbar.Size = new System.Drawing.Size(100, 16);
            // 
            // voltage
            // 
            this.voltage.Location = new System.Drawing.Point(9, 19);
            this.voltage.Name = "voltage";
            this.voltage.Size = new System.Drawing.Size(120, 20);
            this.voltage.TabIndex = 7;
            // 
            // measure
            // 
            this.measure.Location = new System.Drawing.Point(139, 45);
            this.measure.Name = "measure";
            this.measure.Size = new System.Drawing.Size(82, 20);
            this.measure.TabIndex = 8;
            this.measure.Text = "Измерить";
            this.measure.UseVisualStyleBackColor = true;
            this.measure.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Диапазон напряжений";
            // 
            // voltageStep
            // 
            this.voltageStep.Location = new System.Drawing.Point(139, 19);
            this.voltageStep.Name = "voltageStep";
            this.voltageStep.Size = new System.Drawing.Size(82, 20);
            this.voltageStep.TabIndex = 10;
            // 
            // voltageStepLable
            // 
            this.voltageStepLable.AutoSize = true;
            this.voltageStepLable.Location = new System.Drawing.Point(136, 3);
            this.voltageStepLable.Name = "voltageStepLable";
            this.voltageStepLable.Size = new System.Drawing.Size(27, 13);
            this.voltageStepLable.TabIndex = 11;
            this.voltageStepLable.Text = "Шаг";
            // 
            // connect
            // 
            this.connect.Location = new System.Drawing.Point(231, 25);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(91, 21);
            this.connect.TabIndex = 12;
            this.connect.Text = "Подключиться";
            this.connect.UseVisualStyleBackColor = true;
            this.connect.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // importCsv
            // 
            this.importCsv.Location = new System.Drawing.Point(9, 45);
            this.importCsv.Name = "importCsv";
            this.importCsv.Size = new System.Drawing.Size(120, 20);
            this.importCsv.TabIndex = 13;
            this.importCsv.Text = "Импорт в csv";
            this.importCsv.UseVisualStyleBackColor = true;
            this.importCsv.Click += new System.EventHandler(this.importCsv_Click);
            // 
            // ModeSelector
            // 
            this.ModeSelector.Controls.Add(this.VOLTage_tab);
            this.ModeSelector.Controls.Add(this.CURRent_tab);
            this.ModeSelector.Location = new System.Drawing.Point(12, 52);
            this.ModeSelector.Name = "ModeSelector";
            this.ModeSelector.SelectedIndex = 0;
            this.ModeSelector.Size = new System.Drawing.Size(701, 402);
            this.ModeSelector.TabIndex = 14;
            this.ModeSelector.Selected += new System.Windows.Forms.TabControlEventHandler(this.ModeSelector_Selected);
            // 
            // VOLTage_tab
            // 
            this.VOLTage_tab.Controls.Add(this.measure_results);
            this.VOLTage_tab.Controls.Add(this.measure);
            this.VOLTage_tab.Controls.Add(this.importCsv);
            this.VOLTage_tab.Controls.Add(this.voltage);
            this.VOLTage_tab.Controls.Add(this.label2);
            this.VOLTage_tab.Controls.Add(this.voltageStepLable);
            this.VOLTage_tab.Controls.Add(this.voltageStep);
            this.VOLTage_tab.Location = new System.Drawing.Point(4, 22);
            this.VOLTage_tab.Name = "VOLTage_tab";
            this.VOLTage_tab.Padding = new System.Windows.Forms.Padding(3);
            this.VOLTage_tab.Size = new System.Drawing.Size(693, 376);
            this.VOLTage_tab.TabIndex = 0;
            this.VOLTage_tab.Text = "Стабилизация по напряжению (CV)";
            this.VOLTage_tab.UseVisualStyleBackColor = true;
            // 
            // measure_results
            // 
            this.measure_results.AllowUserToDeleteRows = false;
            this.measure_results.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.measure_results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measure_results.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.v,
            this.a,
            this.p});
            this.measure_results.Location = new System.Drawing.Point(227, 6);
            this.measure_results.Name = "measure_results";
            this.measure_results.Size = new System.Drawing.Size(460, 364);
            this.measure_results.TabIndex = 14;
            // 
            // v
            // 
            this.v.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.v.HeaderText = "Напряжение, В";
            this.v.Name = "v";
            this.v.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // a
            // 
            this.a.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.a.HeaderText = "Ток, А";
            this.a.Name = "a";
            // 
            // p
            // 
            this.p.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.p.HeaderText = "Мощность, Вт";
            this.p.Name = "p";
            this.p.Visible = false;
            // 
            // CURRent_tab
            // 
            this.CURRent_tab.Location = new System.Drawing.Point(4, 22);
            this.CURRent_tab.Name = "CURRent_tab";
            this.CURRent_tab.Padding = new System.Windows.Forms.Padding(3);
            this.CURRent_tab.Size = new System.Drawing.Size(693, 376);
            this.CURRent_tab.TabIndex = 1;
            this.CURRent_tab.Text = "Стабилизация по току (CC)";
            this.CURRent_tab.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 479);
            this.Controls.Add(this.ModeSelector);
            this.Controls.Add(this.connect);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.devices);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.scan);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ModeSelector.ResumeLayout(false);
            this.VOLTage_tab.ResumeLayout(false);
            this.VOLTage_tab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measure_results)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button scan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox devices;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar status_progressbar;
        private System.Windows.Forms.TextBox voltage;
        private System.Windows.Forms.Button measure;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox voltageStep;
        private System.Windows.Forms.Label voltageStepLable;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Button importCsv;
        private System.Windows.Forms.TabControl ModeSelector;
        private System.Windows.Forms.TabPage VOLTage_tab;
        private System.Windows.Forms.TabPage CURRent_tab;
        private System.Windows.Forms.DataGridView measure_results;
        private System.Windows.Forms.ToolStripStatusLabel status_logger;
        private System.Windows.Forms.DataGridViewTextBoxColumn v;
        private System.Windows.Forms.DataGridViewTextBoxColumn a;
        private System.Windows.Forms.DataGridViewTextBoxColumn p;
    }
}

